﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interpreter
{
    class ConversionContext

    {
        
        private string conversionResponse = "";
        private string From;
        private string Num;
        private string To;
        private string Ans;


        public ConversionContext(String from,String num,String to)
        {
            From = from;
            Num= num;
            To = to;
         

        }

        public String getFrom
        {
            get
            {
                return From;
            }
            set { }
        }
        public double getNum
        {
            get
            {
                return double.Parse(Num);
            }
            set { }
        }
        public String getTo
        {
            get
            {
                return To;
            }
            set { }
        }
   
        public double getResponse
        {
            get; set;
        }
    }
}
