﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interpreter
{
    class Celcius : IExpression
    {
        public double celcius(double num)
        {
            return num;
        }

        public double fahrenheit(double num)
        {
            return 9 / 5 * num + 32;
        }

        public double kelvin(double num)
        {
            return num + 273.15;
        }
    }
}
