﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Interpreter
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();
        }

        private string convert()
        {
            string from = cmb1.SelectionBoxItem.ToString();
            string to = cmb2.SelectionBoxItem.ToString();
            string num = number.Text.ToString();
            ConversionContext cx = new ConversionContext(from, num, to);
            Celcius cs = new Celcius();
            Fahrenheit fa = new Fahrenheit();
            Kelvin k = new Kelvin();
            if (cx.getFrom=="celcius"){
                if (cx.getTo=="celcius") {
                  cx.getResponse= cs.celcius(cx.getNum);
                  
                }
               else if (cx.getTo=="fahrenheit")
                {
                   cx.getResponse= cs.fahrenheit(cx.getNum);
              
                }
                else if (cx.getTo=="kelvin")
                {
                    cx.getResponse= cs.kelvin(cx.getNum);
           
                }
            }
            else if (cx.getFrom=="fahrenheit")
            {
                if (cx.getTo=="celcius")
                {
                    cx.getResponse= fa.celcius(cx.getNum);
                 
                }
                else if (cx.getTo=="fahrenheit")
                {
                    cx.getResponse= fa.fahrenheit(cx.getNum);
                   
                }
                else if (cx.getTo=="kelvin")
                {
                    cx.getResponse= fa.kelvin(cx.getNum);
                  
                }
            }
            else if (cx.getFrom=="kelvin")
            {
                if (cx.getTo=="celcius")
                {
                    cx.getResponse= k.celcius(cx.getNum);
                  
                }
                else if (cx.getTo=="fahrenheit")
                {
                    cx.getResponse= k.fahrenheit(cx.getNum);
                 
                }
                else if (cx.getTo=="kelvin")
                {
                  
                    cx.getResponse= k.kelvin(cx.getNum);
                  

                }

            }
            return cx.getResponse.ToString();

        }
        //
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            answer.Content = convert(); ;
            
        }
    }
}
