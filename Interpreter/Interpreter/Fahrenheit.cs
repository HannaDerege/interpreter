﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interpreter
{
    class Fahrenheit : IExpression
    {
        public double celcius(double num)
        {
            return (num - 32) * 5 / 9;
        }

        public double fahrenheit(double num)
        {
            return num;
        }

        public double kelvin(double num)
        {
            return (num - 32) * 5 / 9 + 273.15;
        }
    }
}
