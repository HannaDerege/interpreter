﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interpreter
{
    class Kelvin : IExpression
{
      
        public double celcius(double num)
        {
            return num - 273.15;
        }

        public double fahrenheit(double num)
        {
            return (num - 273.15) * 9 / 5 + 32;
        }

        public double kelvin(double num)
        {
            return num;
;        }
    }
}
